package kbl.tmn.taxridtservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import kbl.tmn.taxridtservice.model.Borrow;
import kbl.tmn.taxridtservice.model.view.IBorrow;
import kbl.tmn.taxridtservice.model.view.IBorrowDetails;
import kbl.tmn.taxridtservice.repository.BorrowRepository;


@RestController
public class BorrowRestController {

	@Autowired
    private BorrowRepository borrowRepository;

    @GetMapping("/borrows")
    public List<IBorrow> getBorrows(){
        return this.borrowRepository.findBy();
    }
    
    @GetMapping("/borrows/{id}")
    public Borrow getBorrow(@PathVariable(name="id") Long id){
        return this.borrowRepository.findById(id).get();
    }
    
    @GetMapping("/borrows/details/{id}")
    public IBorrowDetails getBorrowDetails(@PathVariable(name="id") Long id){
        return this.borrowRepository.getById(id).get();
    }
    
    @PostMapping("/borrows")
    public Long addBorrow(@RequestBody Borrow borrow) {
    	return this.borrowRepository.save(borrow).getId();
    }
    
    @PutMapping("/borrows/{id}")
    public Long updateBorrow(@PathVariable(name="id") Long id, @RequestBody Borrow borrow ) {
    	borrow.setId(id);
    	return this.borrowRepository.save(borrow).getId();
    }
    
    @DeleteMapping("/borrows/{id}")
    public boolean deleteBorrow(@PathVariable(name="id") Long id) {
    	//return ( this.borrowRepository.deleteBorrowById(id) > 0 ? true : false);
        this.borrowRepository.deleteById(id);
        return true;
    }
}
