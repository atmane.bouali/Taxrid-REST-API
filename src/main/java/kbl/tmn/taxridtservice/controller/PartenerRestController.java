package kbl.tmn.taxridtservice.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import kbl.tmn.taxridtservice.model.Partener;
import kbl.tmn.taxridtservice.model.PartenerType;
import kbl.tmn.taxridtservice.model.view.IPartener;
import kbl.tmn.taxridtservice.model.view.IPartenerDetails;
import kbl.tmn.taxridtservice.repository.PartenerRepository;

@RestController
public class PartenerRestController {
	
	@Autowired
	private PartenerRepository partenerRepository;
	
	@GetMapping("/parteners")
	public List<IPartener> getParteners(){
		//return this.partenerRepository.findAllParteners();
		return this.partenerRepository.findBy();
	}
	
	@GetMapping("/parteners/{id}")
	public Partener getPartener(@PathVariable(name="id") Long id) {
		return this.partenerRepository.findById(id).get();
		
	}
	
	@GetMapping("/parteners/details/{id}")
	public IPartenerDetails getPartenerDetails(@PathVariable(name="id") Long id) {
		return this.partenerRepository.getById(id).get();
	}
	
	@PostMapping("/parteners")
	public Long addPartener(@RequestBody Partener partener) {
		return this.partenerRepository.save(partener).getId();
		
	}
	
	@PutMapping("/parteners/{id}")
	public Long updatePartener(@PathVariable(name="id") Long id, @RequestBody Partener partener) {
		partener.setId(id);
		return this.partenerRepository.save(partener).getId();
	}
	
	@DeleteMapping("/parteners/{id}")
	public boolean deletePartener(@PathVariable(name="id") Long id) {
		//return ( this.partenerRepository.deletePartenerById(id) > 0 ? true : false);
		this.partenerRepository.deleteById(id);
		return true;
	}
}
