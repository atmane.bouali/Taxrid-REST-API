package kbl.tmn.taxridtservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import kbl.tmn.taxridtservice.model.Repay;
import kbl.tmn.taxridtservice.model.view.IRepay;
import kbl.tmn.taxridtservice.model.view.IRepayDetails;
import kbl.tmn.taxridtservice.repository.RepayRepository;


@RestController
public class RepayRestController {

	@Autowired
    private RepayRepository repayRepository;

    @GetMapping("/repays")
    public List<IRepay> getRepays(){
        return this.repayRepository.findBy();
    }
    @GetMapping("/repays/operation/{id}")
    public List<IRepay> getRepaysOfOperation(@PathVariable(name="id") Long id){
        return this.repayRepository.findByOperation_id(id);
    }
    
    @GetMapping("/repays/{id}")
    public Repay getRepay(@PathVariable(name="id") Long id){
        return this.repayRepository.findById(id).get();
    }
    
    @GetMapping("/repays/details/{id}")
    public IRepayDetails getRepayDetaills(@PathVariable(name="id") Long id){
        return this.repayRepository.getById(id).get();
    }
    
    @PostMapping("/repays")
    public Repay addRepay(@RequestBody Repay repay ) {
    	return this.repayRepository.save(repay);
    }
    
    @PutMapping("/repays/{id}")
    public Repay updateRepay(@PathVariable(name="id") Long id, @RequestBody Repay repay) {
    	repay.setId(id);
    	return this.repayRepository.save(repay);
    }
    
    @DeleteMapping("/repays/{id}")
    public boolean deleteRepay(@PathVariable(name="id") Long id) {
    	this.repayRepository.deleteById(id);
    	return true;
    }
}
