package kbl.tmn.taxridtservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import kbl.tmn.taxridtservice.model.Loan;
import kbl.tmn.taxridtservice.model.view.ILoan;
import kbl.tmn.taxridtservice.model.view.ILoanDetails;
import kbl.tmn.taxridtservice.repository.LoanRepository;


@RestController
public class LoanRestController {

	@Autowired
    private LoanRepository loanRepository;

    @GetMapping("/loans")
    public List<ILoan> getLoans(){
        return this.loanRepository.findBy();
    }
    

    @GetMapping("/loans/{id}")
    public Loan getLoan(@PathVariable(name="id") Long id){
        return this.loanRepository.findById(id).get();
    }
    
    @GetMapping("/loans/details/{id}")
    public ILoanDetails getLoanDetails(@PathVariable(name="id") Long id){
        return this.loanRepository.getById(id).get();
    }
    
    @PostMapping("/loans")
    public Long addLoan(@RequestBody Loan loan) {
    	return this.loanRepository.save(loan).getId();
    }
    
    @PutMapping("/loans/{id}")
    public Long updateLoan(@PathVariable(name="id") Long id, @RequestBody Loan loan ) {
    	loan.setId(id);
    	return this.loanRepository.save(loan).getId();
    }
    
    @DeleteMapping("/loans/{id}")
    public boolean deleteLoan(@PathVariable(name="id") Long id) {
    	this.loanRepository.deleteById(id);
    	return true;
    }
}
