package kbl.tmn.taxridtservice.controller;

import kbl.tmn.taxridtservice.model.Currency;
import kbl.tmn.taxridtservice.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CurrencyRestController {

    @Autowired
    private CurrencyRepository currencyRepository;

    @GetMapping("/currencies")
    public List<Currency> getCurrencies(){
        return this.currencyRepository.findAll();
    }

    @GetMapping("/currencies/{id}")
    public Currency getCurrency(@PathVariable(name="id") Long id){
        return this.currencyRepository.findById(id).get();
    }

    @PostMapping("/currencies")
    public Currency addCurrency(@RequestBody Currency currency){
        return this.currencyRepository.save(currency);
    }

    @PutMapping("/currencies/{id}")
    public Currency updateCurrency( @PathVariable(name="id") Long id, @RequestBody Currency currency){
        currency.setId(id);
        return this.currencyRepository.save(currency);
    }

    @DeleteMapping("/currencies/{id}")
    public boolean deleteCurrency(@PathVariable(name="id") Long id){
        this.currencyRepository.deleteById(id);
        return true;
    }
}
