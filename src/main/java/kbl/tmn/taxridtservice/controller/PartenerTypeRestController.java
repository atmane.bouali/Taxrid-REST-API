package kbl.tmn.taxridtservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import kbl.tmn.taxridtservice.model.PartenerType;
import kbl.tmn.taxridtservice.model.view.IPartenerTypeSubList;
import kbl.tmn.taxridtservice.repository.PartenerTypeRepository;

@RestController
public class PartenerTypeRestController {

	@Autowired
	private PartenerTypeRepository partenerTypeRepository;
	
	@GetMapping("/types")
    public List<PartenerType> getTypes(){
        return this.partenerTypeRepository.findAll();
    }
	
	@GetMapping("/types/sublist")
    public List<IPartenerTypeSubList> getTypesSubList(){
        return this.partenerTypeRepository.getBy();
    }
	
    @GetMapping("/types/{id}")
    public PartenerType getPartenerTypes(@PathVariable(name="id") Long id){
    	return this.partenerTypeRepository.findById(id).get();
    }
    
    @PostMapping("/types")
    public PartenerType addPartenerType(@RequestBody PartenerType partenerType) {
    	return this.partenerTypeRepository.save(partenerType);
    }
    
    @PutMapping("/types/{id}")
    public PartenerType updatePartenerType(@PathVariable(name="id") Long id, @RequestBody PartenerType partenerType) {
    	partenerType.setId(id);
    	return this.partenerTypeRepository.save(partenerType);
    }
    
    @DeleteMapping("/types/{id}")
    public boolean deletePartenerType(@PathVariable(name="id") Long id) {
    	this.partenerTypeRepository.deleteById(id);
    	return true;
    }
}
