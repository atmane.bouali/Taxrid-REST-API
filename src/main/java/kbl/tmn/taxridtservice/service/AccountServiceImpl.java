package kbl.tmn.taxridtservice.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import kbl.tmn.taxridtservice.model.Role;
import kbl.tmn.taxridtservice.model.User;
import kbl.tmn.taxridtservice.repository.RoleRepository;
import kbl.tmn.taxridtservice.repository.UserRepository;

@Service
@Transactional
public class AccountServiceImpl implements AccountService{
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public User saveUser(User user) {
		String hashPW = this.bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(hashPW);
		return this.userRepository.save(user);
	}

	@Override
	public Role saveRole(Role role) {
		return this.roleRepository.save(role);
	}

	@Override
	public void addRoleToUser(String username, String roleName) {
		User user = this.userRepository.findByUsername(username);
		Role role = this.roleRepository.findByRoleName(roleName);
		user.getRoles().add(role);
	}

	@Override
	public User findUserByUsername(String username) {
		
		User r = this.userRepository.findByUsername(username);
		return r;
	}
}
