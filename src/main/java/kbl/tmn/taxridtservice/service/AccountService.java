package kbl.tmn.taxridtservice.service;

import kbl.tmn.taxridtservice.model.Role;
import kbl.tmn.taxridtservice.model.User;

public interface AccountService {
	public User saveUser(User user);
	public Role saveRole(Role role);
	public void addRoleToUser(String username, String roleName);
	public User findUserByUsername(String username);
}
