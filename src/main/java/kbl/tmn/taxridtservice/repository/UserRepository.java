package kbl.tmn.taxridtservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kbl.tmn.taxridtservice.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	public User findByUsername(String username);
}
