package kbl.tmn.taxridtservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import kbl.tmn.taxridtservice.model.PartenerType;
import kbl.tmn.taxridtservice.model.view.IPartenerTypeSubList;

public interface PartenerTypeRepository extends JpaRepository<PartenerType, Long>{
	
	/*@Query("SELECT pt FROM PartenerType pt WHERE pt.removed = false")
    List<PartenerType> findAllPartenerTypes();
	
	@Transactional
	@Modifying
	@Query("UPDATE PartenerType pt SET pt.removed = true WHERE pt.id = :partenerTypeId")
	int deletePartenerTypeById(@Param("partenerTypeId") Long id);
	*/
	List<IPartenerTypeSubList> getBy();
	
}
