package kbl.tmn.taxridtservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kbl.tmn.taxridtservice.model.Operation;

public interface OperationRepository extends JpaRepository<Operation, Long>{

}
