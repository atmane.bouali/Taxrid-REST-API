package kbl.tmn.taxridtservice.repository;

import kbl.tmn.taxridtservice.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {
	
}
