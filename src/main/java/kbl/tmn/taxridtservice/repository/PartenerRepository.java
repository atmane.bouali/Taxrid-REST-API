package kbl.tmn.taxridtservice.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kbl.tmn.taxridtservice.model.Partener;
import kbl.tmn.taxridtservice.model.view.IPartener;
import kbl.tmn.taxridtservice.model.view.IPartenerDetails;

public interface PartenerRepository extends JpaRepository<Partener, Long>{

	List<IPartener> findBy();
	
	Optional<IPartenerDetails> getById(Long id);
	
	/*@Transactional
	@Modifying
	@Query("UPDATE Partener p SET p.removed = true WHERE p.id = :partenerId")
	int deletePartenerById(@Param("partenerId")Long id);*/
}
