package kbl.tmn.taxridtservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kbl.tmn.taxridtservice.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	public Role findByRoleName(String roleName);
}
