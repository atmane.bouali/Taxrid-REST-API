package kbl.tmn.taxridtservice.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kbl.tmn.taxridtservice.model.Borrow;
import kbl.tmn.taxridtservice.model.view.IBorrow;
import kbl.tmn.taxridtservice.model.view.IBorrowDetails;

public interface BorrowRepository extends JpaRepository<Borrow, Long>{
	
	List<IBorrow> findBy();
    
	Optional<IBorrowDetails> getById(Long id);

	/*@Transactional
	@Modifying
	@Query("UPDATE Borrow b SET b.removed = true WHERE b.id = :borrowId")
	int deleteBorrowById(@Param("borrowId")Long id);*/
}