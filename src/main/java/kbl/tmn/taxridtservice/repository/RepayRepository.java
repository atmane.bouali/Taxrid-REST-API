package kbl.tmn.taxridtservice.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kbl.tmn.taxridtservice.model.Repay;
import kbl.tmn.taxridtservice.model.view.IRepay;
import kbl.tmn.taxridtservice.model.view.IRepayDetails;

public interface RepayRepository extends JpaRepository<Repay, Long>{

	List<IRepay> findBy();
	
	List<IRepay> findByOperation_id(Long id);
	
	Optional<IRepayDetails> getById(Long id);
}
