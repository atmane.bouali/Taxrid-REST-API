package kbl.tmn.taxridtservice.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kbl.tmn.taxridtservice.model.Loan;
import kbl.tmn.taxridtservice.model.view.ILoan;
import kbl.tmn.taxridtservice.model.view.ILoanDetails;

public interface LoanRepository extends JpaRepository<Loan, Long>{
	
	List<ILoan> findBy();
	
	Optional<ILoanDetails> getById(Long id);
	
	/*@Transactional
	@Modifying
	@Query("UDDATE Loan l set l.romoved = true WHERE l.id = :loanId")
	int deleteLoanById(@Param("loanId")Long id);*/
}
