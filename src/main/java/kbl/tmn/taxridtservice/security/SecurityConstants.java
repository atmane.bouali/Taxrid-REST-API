package kbl.tmn.taxridtservice.security;

public class SecurityConstants {
	public static final String SECRET = "taxridt@tmn.kbl";
	public static final long EXPIRATION_TIME = 863_000_000;//10 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";	
}