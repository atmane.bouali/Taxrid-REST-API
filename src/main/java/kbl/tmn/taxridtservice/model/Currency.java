package kbl.tmn.taxridtservice.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Currency extends BaseModel {
    private String name;
    private String description;
    
    public Currency() {
    
    }

    public Currency(boolean status, boolean removed, Date createdAt, Date updatedAt, String name, String description) {
		super(status, removed, createdAt, updatedAt);
		this.name = name;
        this.description = description;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}