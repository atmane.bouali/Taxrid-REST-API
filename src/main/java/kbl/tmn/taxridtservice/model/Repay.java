package kbl.tmn.taxridtservice.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Repay extends BaseModel{
	private String title;
	private double amount;
	@Temporal(TemporalType.DATE)
	private Date repayedDate;
	private String comment;
	@Enumerated(EnumType.STRING)
	private PayementMode payementMode;
	@OneToOne
	private Operation operation;
	
	public Repay() {
	}

	public Repay(boolean status, boolean removed, Date createdAt, Date updatedAt, String title, double amount, Date repayedDate, String comment, PayementMode payementMode,
			Operation operation) {
		super(status, removed, createdAt, updatedAt);
		this.title = title;
		this.amount = amount;
		this.repayedDate = repayedDate;
		this.comment = comment;
		this.payementMode = payementMode;
		this.operation = operation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getRepayedDate() {
		return repayedDate;
	}

	public void setRepayedDate(Date repayedDate) {
		this.repayedDate = repayedDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PayementMode getPayementMode() {
		return payementMode;
	}

	public void setPayementMode(PayementMode payementMode) {
		this.payementMode = payementMode;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
}
