package kbl.tmn.taxridtservice.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Operation extends BaseModel{
	private String title;
	private double price;
	private String comment;
	@Temporal(TemporalType.DATE)
	private Date operationDate;
	@Enumerated(EnumType.STRING)
	private PayementMode payementMode;
	@ManyToOne
	private Currency currency;
	@OneToOne
	private Partener partener;
	
	
	
	public Operation() {

	}
	
	public Operation(boolean status, boolean removed, Date createdAt, Date updatedAt, String title, double price, String comment, Date operationDate, PayementMode payementMode,
			Currency currency, Partener partener) {
		super(status, removed, createdAt, updatedAt);
		this.title = title;
		this.price = price;
		this.comment = comment;
		this.operationDate = operationDate;
		this.payementMode = payementMode;
		this.currency = currency;
		this.partener = partener;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public PayementMode getPayementMode() {
		return payementMode;
	}

	public void setPayementMode(PayementMode payementMode) {
		this.payementMode = payementMode;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Partener getPartener() {
		return partener;
	}

	public void setPartener(Partener partener) {
		this.partener = partener;
	}
}
