package kbl.tmn.taxridtservice.model;

public enum PayementMode {
	VIREMENT,
	CHEQUE,
	ESPECE,
	CARTE
}
