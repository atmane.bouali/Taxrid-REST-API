package kbl.tmn.taxridtservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class PartenerType extends BaseModel implements Serializable{
	private String name;
	private String description;
	
	public PartenerType() {
	}
	
	public PartenerType(boolean status, boolean removed, Date createdAt, Date updatedAt, String name, String description) {
		super(status, removed, createdAt, updatedAt);
		this.name = name;
		this.description = description;
	}

	public PartenerType(String partenerTypeName) {
        
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
