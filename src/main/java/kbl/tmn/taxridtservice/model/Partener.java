package kbl.tmn.taxridtservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Partener extends BaseModel implements Serializable{
	private String firstName;
	private String lastName; 
	private String description;
	private String email;
	private String tel;
	private String mobile;
	@OneToOne
	private PartenerType type;
	
	public Partener() {
	}

	public Partener(boolean status, boolean removed, Date createdAt, Date updatedAt, String firstName, String lastName, String description, String email, String tel, String mobile,
			PartenerType type) {
		super(status, removed, createdAt, updatedAt);
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.email = email;
		this.tel = tel;
		this.mobile = mobile;
		this.type = type;
	}
	
	public Partener(String firstName, String lastName, Long typeId, String typeName) {
		//super(status, removed, createdAt, updatedAt);
		this.firstName = firstName;
		this.lastName = lastName;
		this.type.setId(typeId);
		this.type.setName(typeName);
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public PartenerType getType() {
		return type;
	}

	public void setType(PartenerType type) {
		this.type = type;
	}
}
