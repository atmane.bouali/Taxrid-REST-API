package kbl.tmn.taxridtservice.model.view;

import org.springframework.beans.factory.annotation.Value;

public interface IPartenerTypeSubList {
	@Value("#{target.id}")
	Long getId();
	@Value("#{target.name}")
	String getName();
	@Value("#{target.description}")
	String getDescription();
}
