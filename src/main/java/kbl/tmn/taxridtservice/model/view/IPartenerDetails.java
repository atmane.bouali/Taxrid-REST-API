package kbl.tmn.taxridtservice.model.view;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

public interface IPartenerDetails extends IPartener{
	@Value("#{target.description}")
	String getTypeDescription();
	@Value("#{target.removed}")
	boolean isRemoved();
	@Value("#{target.createdAt}")
	Date getCreatedAt();
	@Value("#{target.updatedAt}")
	Date getUpdatedAt();
}
