package kbl.tmn.taxridtservice.model.view;

import org.springframework.beans.factory.annotation.Value;

public interface ILoanDetails extends ILoan{
	@Value("#{target.description}")
	String getDescription();
	@Value("#{target.operation.comment}")
	String getComment();
	
}
