package kbl.tmn.taxridtservice.model.view;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

public interface IBorrowDetails extends IBorrow{
	@Value("#{target.operation.id}")
	Long getOperationId();
	@Value("#{target.operation.partener.id}")
	Long getPartenerId();
	@Value("#{target.operation.comment}")
	String getComment();
	@Value("#{target.operation.createdAt}")
	Date getCreatedAt();
	@Value("#{target.operation.updatedAt}")
	Date getUpdatedAt();
}
