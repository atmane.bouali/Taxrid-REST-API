package kbl.tmn.taxridtservice.model.view;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import kbl.tmn.taxridtservice.model.PayementMode;

public interface IBorrow {
	@Value("#{target.id}")
	Long getId();
	@Value("#{target.operation.title}")
	String getTitle();
	@Value("#{target.operation.price}")
	double getPrice();
	@Value("#{target.operation.operationDate}")
	Date getOperationDate();
	@Value("#{target.operation.payementMode}")
	PayementMode getPayementMode();
	@Value("#{target.operation.currency.name}")
	String getCurrency();
	@Value("#{target.operation.partener.firstName + ' ' + target.operation.partener.lastName}")
	String getPartener();
}
