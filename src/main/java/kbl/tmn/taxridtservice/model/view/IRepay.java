package kbl.tmn.taxridtservice.model.view;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

public interface IRepay {
	@Value("#{target.id}")
	Long getId();
	@Value("#{target.title}")
	String getTitle();
	@Value("#{target.amount}")
	double getAmount();
	@Value("#{target.repayedDate}")
	Date getRepayedDate();
	@Value("#{target.comment}")
	String getComment();
}
