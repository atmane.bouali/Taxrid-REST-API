package kbl.tmn.taxridtservice.model.view;

import org.springframework.beans.factory.annotation.Value;

public interface IPartener {
	@Value("#{target.id}")
	Long getId();
	@Value("#{target.firstName}")
	String getFirstName();
	@Value("#{target.lastName}")
	String getLastName();
	@Value("#{target.description}")
	String getdescription();
	@Value("#{target.status}")
	boolean isStatus();
	@Value("#{target.email}")
	String getEmail();
	@Value("#{target.tel}")
	String getTel(); 
	@Value("#{target.mobile}")
	String getMobile();
	@Value("#{target.type.name}")
	String getType();
}
