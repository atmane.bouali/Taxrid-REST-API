package kbl.tmn.taxridtservice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.ietf.jgss.Oid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import kbl.tmn.taxridtservice.model.Borrow;
import kbl.tmn.taxridtservice.model.Currency;
import kbl.tmn.taxridtservice.model.Loan;
import kbl.tmn.taxridtservice.model.Operation;
import kbl.tmn.taxridtservice.model.Partener;
import kbl.tmn.taxridtservice.model.PartenerType;
import kbl.tmn.taxridtservice.model.PayementMode;
import kbl.tmn.taxridtservice.model.Repay;
import kbl.tmn.taxridtservice.model.Role;
import kbl.tmn.taxridtservice.model.User;
import kbl.tmn.taxridtservice.model.view.IPartener;
import kbl.tmn.taxridtservice.repository.BorrowRepository;
import kbl.tmn.taxridtservice.repository.CurrencyRepository;
import kbl.tmn.taxridtservice.repository.LoanRepository;
import kbl.tmn.taxridtservice.repository.OperationRepository;
import kbl.tmn.taxridtservice.repository.PartenerRepository;
import kbl.tmn.taxridtservice.repository.PartenerTypeRepository;
import kbl.tmn.taxridtservice.repository.RepayRepository;
import kbl.tmn.taxridtservice.repository.RoleRepository;
import kbl.tmn.taxridtservice.repository.UserRepository;
import kbl.tmn.taxridtservice.service.AccountService;

@SpringBootApplication
public class TaxridtServiceApplication implements CommandLineRunner{
	
	@Autowired
	private PartenerRepository partenerRepository;
	@Autowired
	private PartenerTypeRepository partenerTypeRepository;
	@Autowired
	private OperationRepository operationRepository; 
	@Autowired
	private BorrowRepository borrowRepository;
	@Autowired
	private LoanRepository loanRepository;
	@Autowired
	private CurrencyRepository currencyRepository;
	@Autowired
	private RepayRepository repayRepository;
	@Autowired
	private AccountService accountService;
	
	@Bean
	public BCryptPasswordEncoder getBCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public static void main(String[] args) {

		SpringApplication.run(TaxridtServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*Role r1 = new Role("ADMIN");
		Role r2 = new Role("USER");
		Role r3 = new Role("VISITOR");
		r1 = this.accountService.saveRole(r1);
		r2 = this.accountService.saveRole(r2);
		r3 = this.accountService.saveRole(r3);
		
		Collection<Role> role1 = new ArrayList<>();
		Collection<Role> role2 = new ArrayList<>();
		role1.add(r1);
		role1.add(r2);
		role1.add(r3);
		role2.add(r2);
		role2.add(r3);
		
		User u1 = new User("bouali.atmane06@gmail.com", "atmane", null, null, role1);
		User u2 = new User("katia.aouchar06@gmail.com", "katia", null, null, role2);
		this.accountService.saveUser(u1);
		this.accountService.saveUser(u2);*/
		
//		PartenerType type1 = this.partenerTypeRepository.findById(new Long(1)).get();
//		Partener p1 = new Partener(true, false, new Date(), new Date(), "atmane", "bouali", "moi", "atmane.bouali@gmail.com", "0664419685", "0665654345", type1, null);
//		//Partener p2 = new Partener(true, false, new Date(), new Date(), "aaaa", "bbbb", "aaaa bbbb cccc", "a@b.d", "0675654342", "06777777", type1, null); 
//		IPartener ip = (IPartener) this.partenerRepository.save(p1);
//		System.out.println(ip.getId()+" "+ ip.getFirstName()+" "+ ip.getLastName()+" "+ ip.getdescription()+" "+ ip.getType()+" "+ ip.isStatus()+" "+ ip.getEmail()+" "+ ip.getTel()+" "+ ip.getMobile());
		
		
		/*PartenerType type1 = new PartenerType(true,false,new Date(), new Date(),"Clients", "Mes clients", null);
		PartenerType type2 = new PartenerType(true,false,new Date(), new Date(),"Fournisseurs", "Mes fournisseurs", null);
		type1 = this.partenerTypeRepository.save(type1);
		type2 = this.partenerTypeRepository.save(type2);
		
		Partener p1 = new Partener(true, false, new Date(), new Date(), "atmane", "bouali", "moi", "atmane.bouali@gmail.com", "0664419685", "0665654345", type2);
		Partener p2 = new Partener(true, false, new Date(), new Date(), "mohend", "hammi", "ihamiyen", "mohend.hammi@gmail.com", "0664419682", "0665654345", type2);
		Partener p3 = new Partener(true, false, new Date(), new Date(), "katia", "aouchar", "ma femme", "katia.aouchar@gmail.com", "0664419683", "0665654345", type2);
		Partener p4 = new Partener(true, false, new Date(), new Date(), "fares", "tafinine", "mon ami", "fares.tafinine@gmail.com", "0664419688", "0665654345", type1 );
		Partener p5 = new Partener(true, false, new Date(), new Date(), "juba", "mahdi", "ami famille", "juba.mehdi@gmail.com", "0664419680", "0665654345", type1 );
		Partener p6 = new Partener(false, false, new Date(), new Date(), "farida", "hadouche", "mama", "farida.hadouche@gmail.com", "0123456789", "0675654345", type1 ); 
		this.partenerRepository.save(p1);
		this.partenerRepository.save(p2);
		this.partenerRepository.save(p3);
		this.partenerRepository.save(p4);
		this.partenerRepository.save(p5);
		this.partenerRepository.save(p6);*/
		
		/*List<PartenerType> partenerTypes = this.partenerTypeRepository.findAllPartenerTypes();
		
		partenerTypes.forEach(pt->System.out.println(pt.getId()+" : "+pt.getName()+" => Supprimer ? = "+pt.isRemoved()));
		
		this.partenerTypeRepository.deletePartenerTypeById(new Long(1));
		
		partenerTypes = this.partenerTypeRepository.findAllPartenerTypes();
		
		*/
		
		/*
		Partener partener1 = this.partenerRepository.findById(new Long(1)).get(); 
		Currency currency1 = this.currencyRepository.findById(new Long(1)).get();
		
		Operation operation1 = new Operation(true, false, new Date(), new Date(), "achat yves rocher", 30.0, "Achat YvesRocher", new Date(), PayementMode.CARTE, currency1, partener1, null);
		
		Borrow borrow1 = new Borrow(operation1);
		
		Repay repay1 = new Repay(true, false, new Date(), new Date(), "repay 1", 15.0, new Date(), "1er remboursement", PayementMode.CARTE, operation1);
		Repay repay2 = new Repay(true, false, new Date(), new Date(), "repay 2", 10.0, new Date(), "2er remboursement", PayementMode.CARTE, operation1);
		
		operation1 = this.operationRepository.save(operation1);
		borrow1 = this.borrowRepository.save(borrow1);
		
		repay1 = this.repayRepository.save(repay1);
		repay2 = this.repayRepository.save(repay2);
		*/
	
		//List<Borrow> borrows = this.borrowRepository.findAllBorrows();
		//System.out.println(borrows.get(0).getId() + " "+ borrows.get(0).getOperation().getPartener().getFirstName());
	}
	
	/*public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api").allowedOrigins("http://localhost:4200");
            }
        };
    }*/
}
